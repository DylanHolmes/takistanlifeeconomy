package com.dylanholmes.takistanlife.economy;

import com.dylanholmes.takistanlife.TakistanLifeEconomy;
import java.util.HashMap;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

/**
 *
 * @author holmd834
 */
public class Bank {

    private long totalAmount;
    private static HashMap<String, BankAccount> accounts = new HashMap<>();
    private static TakistanLifeEconomy tle = TakistanLifeEconomy.getInstance();

    public Bank() {
        loadAllAccounts();
        loadTotalAmount();
    }

    private void loadTotalAmount() {
        for (BankAccount account : accounts.values()) {
            totalAmount += account.getMoney();
        }
    }

    public static BankAccount getAccount(String name) {
        return accounts.get(name);
    }

    public static BankAccount getAccount(Player player) {
        return getAccount(player.getName());
    }

    public static void addAccount(BankAccount account) {
        accounts.put(account.getAccountHolder(), account);
    }

    public static void addAccount(String name, float money, boolean insurance) {
        BankAccount account = new BankAccount(name, money);
        account.setHasInsurance(insurance);
        accounts.put(name, account);
    }

    public static void loadAllAccounts() {
        ConfigurationSection section = tle.getDataFile().getConfigurationSection("accounts");
        for (String keys : section.getKeys(false)) {
            BankAccount acc = new BankAccount(keys,
                    tle.getDataFile().getLong("accounts." + keys + ".money"));
            acc.setHasInsurance(tle.getDataFile().getBoolean("account." + keys + ".insurance"));
            addAccount(acc);
        }
    }

    public static void saveAllAccounts() {
        for (BankAccount account : accounts.values()) {
            tle.getDataFile().set("accounts." + account.getAccountHolder()
                    + ".money", account.getMoney());
            tle.getDataFile().set("accounts." + account.getAccountHolder()
                    + ".insurance", account.getHasInsurance());
        }
        tle.saveDataFile();
    }
}
