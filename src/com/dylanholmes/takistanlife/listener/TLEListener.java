/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dylanholmes.takistanlife.listener;

import com.dylanholmes.takistanlife.TakistanLifeEconomy;
import com.dylanholmes.takistanlife.economy.Bank;
import com.dylanholmes.takistanlife.economy.BankAccount;
import com.dylanholmes.takistanlife.shops.ShopManager;
import com.dylanholmes.takistanlife.shops.ShopNPC;
import com.topcat.npclib.entity.HumanNPC;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author holmd834
 */
public class TLEListener implements Listener {

    private TakistanLifeEconomy tle;

    public TLEListener(TakistanLifeEconomy tle) {
        this.tle = tle;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        Inventory inv = event.getInventory();
        if (!ShopManager.invShopMap.containsKey(inv)) {
            return;
        }
        ItemStack stack = event.getCurrentItem();
        if (stack.getType() == null) {
            return;
        }
        Material mat = stack.getType();
        ShopNPC shop = ShopManager.invShopMap.get(inv);
        event.setCancelled(true);
        boolean action = event.isShiftClick() && event.isLeftClick();
        BankAccount account = Bank.getAccount(player.getName());
        float money = account.getMoney();
        float cost = shop.getPrice(mat);

        if (action) {
            int amount = shop.getAmountPer(mat).intValue();
            float costForOne = cost / amount;
            if (cost > money) {
                if (money > costForOne) {
                    int bought = -1;
                    float i = 0;
                    while (i <= money) {
                        i += costForOne;
                        bought++;
                    }
                    account.subtractMoney(i);
                    player.getInventory().addItem(new ItemStack(mat, bought));
                    player.sendMessage(ChatColor.GOLD + "You have bought " + bought + " " + mat.name() + " for " + i + "$.");
                    return;
                }
                player.sendMessage(ChatColor.GREEN + "You do not have enough money!");
                return;
            }
            account.subtractMoney(cost);
            player.getInventory().addItem(new ItemStack(mat, amount));
            player.sendMessage(ChatColor.GOLD + "You have bought " + amount + " " + mat.name() + " for " + cost + "$.");
        } else {
            player.sendMessage(ChatColor.GREEN + "The price of " + shop.getAmountPer(mat) + mat.name() + " cost " + cost + "$.");
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onShopInteract(PlayerInteractEntityEvent event) {
        if (!(event.getRightClicked() instanceof Player)) {
            return;
        }

        if (!tle.nm.isNPC(event.getRightClicked())) {
            return;
        }
        HumanNPC npc = (HumanNPC) tle.nm.getNPC(((HumanEntity) event.getRightClicked()).getName());
        ShopManager.npcShopMap.get(npc).openShopUI(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!player.hasPlayedBefore()) {
            BankAccount account = new BankAccount(player.getName(), 1000);
            Bank.addAccount(account);
        }
        if (/*TODO: Make a way to set/get a player's activity so we can give them money.*/10 - System.currentTimeMillis() >= 86400000) {
            //TODO: ^
        }
    }
}
