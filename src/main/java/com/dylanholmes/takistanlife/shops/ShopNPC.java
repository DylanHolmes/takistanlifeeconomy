/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dylanholmes.takistanlife.shops;

import com.topcat.npclib.entity.HumanNPC;
import com.topcat.npclib.nms.NPCEntity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author holmd834
 */
public class ShopNPC {

    private Location loc;
    private int size;
    private int id;
    private HumanNPC npc;
    private Inventory inv;
    private List<Material> allowedItems = new ArrayList<>();
    private Map<Material, LinkedList<Float>> itemMap = new HashMap<>();

    public ShopNPC(HumanNPC npc, Location loc, int size) {
        this.loc = loc;
        this.size = size;
        this.npc = npc;
        ShopManager.npcShopMap.put(npc, this);
    }

    public Location getLocation() {
        return loc;
    }

    public int getSize() {
        return size;
    }

    public void openShopUI(Player player) {
        inv = Bukkit.createInventory(player, size, npc.getName());
        Set<Material> set = itemMap.keySet();
        int i = 0;
        updateInventory();
        player.openInventory(inv);
    }

    public String getName() {
        return npc.getName();
    }

    public void updateInventory() {
        Set<Material> set = itemMap.keySet();
        int i = 0;
        while (set.iterator().hasNext()) {
            Material mat = set.iterator().next();
            if (getAmount(mat) < 1) {
                continue;
            }
            inv.setItem(i, new ItemStack(mat, getAmount(mat).intValue()));
            i++;
        }
        ShopManager.invShopMap.put(inv, this);

    }

    public Float getPrice(Material mat) {
        return itemMap.get(mat).get(0);
    }

    public Float getAmountPer(Material mat) {
        return itemMap.get(mat).get(2);
    }

    public Float getAmount(Material mat) {
        return itemMap.get(mat).get(1);
    }

    public Inventory getInv() {
        return inv;
    }

    public void setInv(Inventory inv) {
        this.inv = inv;
    }
}
