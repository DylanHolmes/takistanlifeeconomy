package com.dylanholmes.takistanlife.shops;

import com.dylanholmes.takistanlife.TakistanLifeEconomy;
import com.topcat.npclib.entity.HumanNPC;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author holmd834
 */
public class ShopManager {

    public static Map<HumanNPC, ShopNPC> npcShopMap = new HashMap<>();
    public static Map<Inventory, ShopNPC> invShopMap = new HashMap<>();
    private static TakistanLifeEconomy tle;
    private static YamlConfiguration file;

    public ShopManager(TakistanLifeEconomy tle) {
        ShopManager.tle = tle;
        file = tle.getDataFile();

    }

    public ShopNPC getShop(HumanNPC npc) {
        return npcShopMap.get(npc);
    }

    public ShopNPC getShop(Inventory inv) {
        return invShopMap.get(inv);
    }

    public static void saveNPCs() {
        for (ShopNPC npcs : npcShopMap.values()) {
            String name = npcs.getName();
            //Location
            Location loc = npcs.getLocation();
            file.set("shops.npcs." + name + ".location.x", loc.getX());
            file.set("shops.npcs." + name + ".location.y", loc.getY());
            file.set("shops.npcs." + name + ".location.z", loc.getZ());
            file.set("shops.npcs." + name + ".location.world", loc.getWorld().getName());
            //Items
            Inventory inv = npcs.getInv();
            for (int i = 0; i < npcs.getSize(); i++) {
                if (i == 0) {
                    file.set("shops.npcs." + name + ".items.size", npcs.getSize());
                }
                ItemStack stack = inv.getItem(i);
                file.set("shops.npcs.", file);
                file.set("shops.npcs." + name + ".items." + i
                        + "." + stack.getType().name() + ".amount", stack.getAmount());
            }
        }
        tle.saveDataFile();
    }

    public static void loadNPCs() {
        ConfigurationSection section = file.getConfigurationSection("shops.npcs");
        for (Iterator<String> it = section.getKeys(false).iterator(); it.hasNext();) {
            String name = it.next();
            World world = Bukkit.getWorld(file.getString("shops.npcs." + name + ".location.world"));
            int x = file.getInt("shops.npcs." + name + ".location.x");
            int y = file.getInt("shops.npcs." + name + ".location.y");
            int z = file.getInt("shops.npcs." + name + ".location.z");
            Location loc = new Location(world, x, y, z);
            HumanNPC hNPC = (HumanNPC) tle.nm.spawnHumanNPC(name, loc);
            ShopNPC npc = new ShopNPC(hNPC, loc, file.getInt("shops.npcs." + name + ".items.size"));
        }
    }
}
