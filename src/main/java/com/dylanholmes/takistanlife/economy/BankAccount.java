package com.dylanholmes.takistanlife.economy;

import com.dylanholmes.takistanlife.TakistanLifeEconomy;
import com.killersmurf.takistanlifedivisions.player.TLDPlayer;
import com.killersmurf.takistanlifedivisions.util.PlayerUtil;
import com.killersmurf.takistanlifegangs.GangUtil;
import com.killersmurf.takistanlifegangs.gangs.GangMember;

/**
 *
 * @author holmd834
 */
public class BankAccount {

    private String accountHolder;
    private float money;
    private float multiplier;
    private boolean hasInsurance;

    public BankAccount(String accountHolder, float money) {
        this.accountHolder = accountHolder;
        this.money = money;
    }

    public boolean getHasInsurance() {
        return hasInsurance;
    }

    public void setHasInsurance(boolean hasInsurance) {
        this.hasInsurance = hasInsurance;
    }

    public BankAccount(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public float getMoney() {
        return money;
    }

    public float getMultiplier() {
        if (getHasInsurance()) {
            multiplier += .01f;
        }
        TLDPlayer player = PlayerUtil.getPlayer(accountHolder);
        if (player.getKarma() < 2) {
            multiplier += .02f;
        }

        GangMember member = GangUtil.getMember(accountHolder);
        if (member.getGang() != null) {
            multiplier += .02f;
        }

        if (member.getPrivileges() > 1) {
            multiplier += .01f;
        }

        if (member.getGang().getChunks().size() > 3) {
            multiplier += .01f;
        }

        return multiplier;
    }

    public void subtractMoney(float amt) {
        addMoney(-amt);
    }

    public void addMoney(float amt) {
        money += amt;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public void load() {
        money = (float) TakistanLifeEconomy.getInstance().getDataFile().getDouble("accounts." + accountHolder);
    }

    public void save() {
        TakistanLifeEconomy.getInstance().getDataFile().set("accounts." + accountHolder, money);
    }
}
