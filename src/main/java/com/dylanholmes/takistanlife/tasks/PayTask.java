/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dylanholmes.takistanlife.tasks;

import com.dylanholmes.takistanlife.economy.Bank;
import com.dylanholmes.takistanlife.economy.BankAccount;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 *
 * @author holmd834
 */
public class PayTask implements Runnable {

    @Override
    public void run() {
        for (Player players : Bukkit.getOnlinePlayers()) {
            BankAccount account = Bank.getAccount(players);
            float toAdd;
            toAdd = (account.getMoney() * account.getMultiplier());
            account.addMoney(500 + toAdd);
        }
    }
}