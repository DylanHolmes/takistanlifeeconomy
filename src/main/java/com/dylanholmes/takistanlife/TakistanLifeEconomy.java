package com.dylanholmes.takistanlife;

import com.dylanholmes.takistanlife.commands.MoneyCommand;
import com.dylanholmes.takistanlife.economy.Bank;
import com.dylanholmes.takistanlife.shops.ShopManager;
import com.dylanholmes.takistanlife.tasks.PayTask;
import com.killersmurf.takistanlifecore.util.TakistanCommand;
import com.topcat.npclib.NPCManager;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author holmd834
 */
public class TakistanLifeEconomy extends JavaPlugin {

    private static TakistanLifeEconomy instance;
    public NPCManager nm;
    private Map<String, TakistanCommand> commandMap = new HashMap<>();
    private YamlConfiguration dataFile;
    public static final Logger LOGGER = Logger.getLogger("Minecraft");
    private Bank bank;

    @Override
    public void onEnable() {
        loadDataFile();
        instance = this;
        bank = new Bank();
        Bank.loadAllAccounts();
        nm = new NPCManager(this);
        ShopManager.loadNPCs();
        //Commands
        commandMap.put("money", new MoneyCommand());
        //End of Commands
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new PayTask(), 0x3cl, 0x1770l);
    }

    @Override
    public void onDisable() {
        ShopManager.saveNPCs();
        Bank.saveAllAccounts();
        saveDataFile();
        saveConfig();
    }

    public static TakistanLifeEconomy getInstance() {
        return instance;
    }

    public YamlConfiguration loadDataFile() {
        File df = new File(this.getDataFolder().toString() + File.separator + "data.yml");

        if (!df.exists()) {
            try {
                df.createNewFile();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, "Could not create the data file!", ex);
            }
        }
        dataFile = YamlConfiguration.loadConfiguration(df);
        return dataFile;
    }

    public YamlConfiguration getDataFile() {
        return dataFile;
    }

    public void saveDataFile() {
        File df = new File(this.getDataFolder().toString() + File.separator + "data.yml");
        try {
            dataFile.save(df);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Could not save the data!", ex);
        }
    }
}
