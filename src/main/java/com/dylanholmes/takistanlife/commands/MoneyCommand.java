/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dylanholmes.takistanlife.commands;

import com.dylanholmes.takistanlife.economy.Bank;
import com.killersmurf.takistanlifecore.util.TakistanCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author holmd834
 */
public class MoneyCommand extends TakistanCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("DU BIST NICHT EIN SPIELER!");
        }
        Player player = (Player) sender;
        player.sendMessage(ChatColor.AQUA + "You have $" + Bank.getAccount(player).getMoney() + ".");
    }
}
